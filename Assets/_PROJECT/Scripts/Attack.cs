using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
    public GameObject attackPrefab;
    public float shootStrength;
    public float shootInterval;

    private Coroutine coroutine;

    public void ShootLeft()
    {
        if (coroutine != null)
            return;
        // shoot key is pressed - spawning a bomb
        coroutine = StartCoroutine(ShootCoroutine(true));
    }

    public void ShootRight()
    {
        if (coroutine != null)
            return;
        // shoot key is pressed - spawning a bomb
        coroutine = StartCoroutine(ShootCoroutine(false));
    }

    private IEnumerator ShootCoroutine(bool shootLeft)
    {
        GameObject bomb = Instantiate(attackPrefab, this.transform.position, Quaternion.identity, this.transform);
        Rigidbody2D bombBody = bomb.GetComponent<Rigidbody2D>();

        if (shootLeft == true)
        {
            bombBody.AddForce(new Vector2(-1,1)*shootStrength, ForceMode2D.Impulse);
        }
        else
        {
            bombBody.AddForce(new Vector2(1,1)*shootStrength, ForceMode2D.Impulse);
        }
        
        yield return new WaitForSeconds(0.5f);

        bomb.GetComponent<CircleCollider2D>().enabled = true;

        yield return new WaitForSeconds(shootInterval);
        coroutine = null;
    }
}
