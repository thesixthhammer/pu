using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReturnToMainMenu : MonoBehaviour
{
    void Start()
    {
        StartCoroutine(ReturnToMainMenuCoroutine());
    }
    IEnumerator ReturnToMainMenuCoroutine()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("Menu");
    }
}
