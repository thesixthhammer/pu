using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Damager : MonoBehaviour
{
    public GameObject gameObjectToSpawnOnDamage;
    public float attackStrength;
    public float knockbackImpulse;


    public void OnCollisionEnter2D(Collision2D other)
    {
        Mortality mortality = other.collider.gameObject.GetComponent<Mortality>();
        if (mortality != null)
        {
            mortality.TakeHP(attackStrength);
            
            // spawn prefab if you have one
            if(gameObjectToSpawnOnDamage != null)
                Instantiate(gameObjectToSpawnOnDamage,
                    new Vector3(other.contacts[0].point.x, other.contacts[0].point.y, 0),
                    Quaternion.identity,
                    null);
            
            
            if (this.gameObject.transform.position.x < other.gameObject.transform.position.x)
            {
                other.rigidbody.AddForce(new Vector2(1,1)*knockbackImpulse, ForceMode2D.Impulse);
            }
            else
            {
                other.rigidbody.AddForce(new Vector2(-1,1)*knockbackImpulse, ForceMode2D.Impulse);
            }

        }
    }
}
