using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follower : MonoBehaviour
{
    public bool followX, followY, followZ;
    public GameObject followGO;
    public Vector3 offset;
    // Update is called once per frame
    void Update()
    {
        float x = (followX) ? followGO.transform.position.x+offset.x : offset.x;
        float y = (followY) ? followGO.transform.position.y+offset.y : offset.y;
        float z = (followZ) ? followGO.transform.position.z+offset.z : offset.z;
        
        this.transform.position = new Vector3(x,y,z);
    }
}
