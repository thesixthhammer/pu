using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    public float knockbackForceStrength;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "chicken")
        {
            Rigidbody2D chickenBody = other.gameObject.GetComponent<Rigidbody2D>();
            if (this.gameObject.transform.position.x < other.gameObject.transform.position.x)
            {
                chickenBody.AddForce(new Vector2(1,1)*knockbackForceStrength, ForceMode2D.Impulse);
            }
            else
            {
                chickenBody.AddForce(new Vector2(-1,1)*knockbackForceStrength, ForceMode2D.Impulse);
            }
        }
        Destroy(this.gameObject);
    }
}
