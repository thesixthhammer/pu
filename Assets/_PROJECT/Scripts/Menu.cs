using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public float bounceSpeed = 1f;
    public TextMeshProUGUI menuText;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(AnimateMenuText());
    }

    void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            // load game scene
            SceneManager.LoadScene("Game");
        }
    }
    
    IEnumerator AnimateMenuText()
    {
        while (true)
        {
            // zoom 1 => 2
            for (float scale = 1f; scale < 1.3f; scale += Time.fixedDeltaTime * bounceSpeed)
            {
                menuText.gameObject.transform.localScale = new Vector3(scale, scale, 1);
                yield return new WaitForFixedUpdate();
            }

            // zoom 2 => 1
            for (float scale = 1.3f; scale > 1f; scale -= Time.fixedDeltaTime * bounceSpeed)
            {
                menuText.gameObject.transform.localScale = new Vector3(scale, scale, 1);
                yield return new WaitForFixedUpdate();
            }
        }
    }
}
