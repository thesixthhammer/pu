using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HighscoreSet : MonoBehaviour
{
    public TextMeshProUGUI highScoreText;
    // Start is called before the first frame update
    void Start()
    {
        highScoreText.text = PlayerPrefs.HasKey("highscore") ? PlayerPrefs.GetInt("highscore") + "" : "0";
    }
}
