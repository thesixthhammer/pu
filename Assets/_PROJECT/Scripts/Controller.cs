using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms.VisualStyles;
using UnityEngine;
using Random = UnityEngine.Random;

public class Controller : MonoBehaviour
{
    public GameObject dieText;
    public AudioSource dashSfx;
    public AudioSource[] attackSfx;
    public KeyCode moveLeftKey, moveRightKey, jumpKey, attackKey, dashKey;
    public float forceStrength = 1;
    public float jumpStrength = 1;
    public float dashStrength = 1;
    public float dashInterval = 0;
    public Animator animator;
    private Rigidbody2D rigidBody;
    private SpriteRenderer spriteRenderer;
    public Mortality mortality;
    public Damager damager;
    private bool moveLeft, moveRight, jump;

    private bool isInTheAir;

    private Coroutine dashCoroutine;
    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        // move right input logic
        moveRight = Input.GetKey(moveRightKey);
        
        // move left input logic
        moveLeft = Input.GetKey(moveLeftKey);
        
        // jump input logic
        if (Input.GetKeyDown(jumpKey))
        {
            jump = true;
        }
        
        // attack
        if (Input.GetKeyDown(attackKey))
        {
            int index = Random.Range(0, attackSfx.Length);
            attackSfx[index].pitch = Random.Range(0.8f, 1.2f);
            attackSfx[index].Play();
            animator.SetTrigger("Slash");
            
        }
        // dash
        if (Input.GetKeyDown(dashKey) && dashCoroutine == null)
        {
            dashSfx.pitch = Random.Range(0.8f, 1.2f);
            dashSfx.Play();
            dashCoroutine = StartCoroutine(DashCoroutine());
        }
        
        
        // update animator
        if (moveRight || moveLeft)
        {
            animator.SetInteger("State",2);
        }
        else
        {
            animator.SetInteger("State",0);
        }
    }

    IEnumerator DashCoroutine()
    {
        
        animator.SetTrigger("Jab");
        damager.gameObject.SetActive(true);
        rigidBody.AddForce(new Vector2(moveLeft ? -1 : 1,0)*dashStrength, ForceMode2D.Impulse);
        mortality.enabled = false;
        yield return new WaitForSeconds(0.5f);
        mortality.enabled = true;
        damager.gameObject.SetActive(false);
        yield return new WaitForSeconds(dashInterval);
        dashCoroutine = null;
    }
    void FixedUpdate()
    {
        if (moveRight == true)
        {
            rigidBody.AddForce(new Vector2(1,0)*forceStrength);
            animator.gameObject.transform.localScale = new Vector3(1, 1, 1);
;            // move right
        }
        else if (moveLeft == true)
        {
            rigidBody.AddForce(new Vector2(-1,0)*forceStrength);
            animator.gameObject.transform.localScale = new Vector3(-1, 1, 1);
            // move left
        }

        if (jump == true && isInTheAir == false)
        {
            rigidBody.AddForce(new Vector2(0,1)*jumpStrength,ForceMode2D.Impulse);
            jump = false;
            isInTheAir = true;
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        isInTheAir = false;
    }


    private void OnDestroy()
    {
        dieText.SetActive(true);
    }
}
