using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawner : MonoBehaviour
{
    public GameObject prefabToSpawn;
    public float intervalToSpawn;


    private void Start()
    {
        StartCoroutine(Spawn());
    }

    IEnumerator Spawn()
    {
        while (true)
        {
            Instantiate(prefabToSpawn, this.transform.position, Quaternion.identity, null);
            yield return new WaitForSeconds(intervalToSpawn);
        }
    }
}
