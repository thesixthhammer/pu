using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mortality : MonoBehaviour
{
    public static int totalEnemiesKilled;

    public bool dontCountAsKill = false;
    public float hp;
    [HideInInspector]
    public float maxHP;

    public Animator animator;
    public string animatorTrigger;


    public List<SpriteRenderer> spriteRenderers; 
    
    private void Start()
    {
        this.maxHP = hp;
    }

    public IEnumerator Blink()
    {
        foreach (var spriteRenderer in spriteRenderers)
        {
            spriteRenderer.color = Color.red;
        }
        yield return new WaitForSeconds(0.25f);
        foreach (var spriteRenderer in spriteRenderers)
        {
            spriteRenderer.color = Color.white;
        }
    }
    public void TakeHP(float hp)
    {
        if (animator != null)
        {
            animator.SetTrigger(animatorTrigger);
        }
        this.hp -= hp;
        StartCoroutine(Blink());
        if (this.hp <= 0)
        {
            if(dontCountAsKill == false)
                totalEnemiesKilled++;

            if (PlayerPrefs.HasKey("highscore") == false)
            {
                PlayerPrefs.SetInt("highscore",totalEnemiesKilled);
            }else if (PlayerPrefs.GetInt("highscore") < totalEnemiesKilled)
            {
                PlayerPrefs.SetInt("highscore",totalEnemiesKilled);
            }
            
            Destroy(this.gameObject);
        }

    }
    public void GiveHP(float hp)
    {
        this.hp += hp;
        if (this.hp > maxHP)
            this.hp = maxHP;
    }
}
