using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Mortality mortality;
    public Image healthBar;

    private float healthBarInitialSize;
    private void Start()
    {
        healthBarInitialSize = healthBar.rectTransform.sizeDelta.x;
    }

    // Update is called once per frame
    void Update()
    {
        float percentLeft = mortality.hp / mortality.maxHP;
        healthBar.rectTransform.sizeDelta = 
            new Vector2(healthBarInitialSize*percentLeft, healthBar.rectTransform.sizeDelta.y);
    }
}
