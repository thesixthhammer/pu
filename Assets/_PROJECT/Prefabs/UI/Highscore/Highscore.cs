using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Highscore : MonoBehaviour
{
    public HorizontalLayoutGroup hlg;
    public TextMeshProUGUI killsNumberText;
    public Transform highscoreHolder;
    public float bounceUpSpeed = 4f;
    public float bounceDownSpeed = 2f;
    private int _lastEnemiesKillCount = -1;

    void Start()
    {
        StartCoroutine(FixHorizontalLayoutGroup());
    }

    IEnumerator FixHorizontalLayoutGroup()
    {
        hlg.enabled = false;
        yield return null; // waits for next Update
        hlg.enabled = true;
    }
    // Update is called once per frame
    void Update()
    {
        if (Mortality.totalEnemiesKilled != _lastEnemiesKillCount)
        {
            StartCoroutine(UpdateKillCounter());
        }
    }

    IEnumerator UpdateKillCounter()
    {
        killsNumberText.text = Mortality.totalEnemiesKilled+"";
        _lastEnemiesKillCount = Mortality.totalEnemiesKilled;

        // zoom 1 => 2
        for (float scale = 1f; scale < 2f; scale += Time.fixedDeltaTime*bounceUpSpeed)
        {
            highscoreHolder.localScale = new Vector3(scale, scale, 1);
            yield return new WaitForFixedUpdate();
        }
        
        // zoom 2 => 1
        for (float scale = 2f; scale > 1f; scale -= Time.fixedDeltaTime*bounceDownSpeed)
        {
            highscoreHolder.localScale = new Vector3(scale, scale, 1);
            yield return new WaitForFixedUpdate();
        }
    }
}
