using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemyPoints : MonoBehaviour
{
    public TextMeshProUGUI enemyPointsNumber;
    public GameObject chickPrefab, chickHardPrefab, chickenPrefab;
    // Start is called before the first frame update

    public GameObject leftPos, rightPos;
    private float points;
    void Start()
    {
        StartCoroutine(GivePoints());
    }

    IEnumerator GivePoints()
    {
        while (true)
        {
            points++;
            if (points > 16)
                points = 16;
            yield return new WaitForSeconds(1f);
        }
    }
    // Update is called once per frame
    void Update()
    {
        
        Vector3 pos = (Random.Range(1, 100) > 50) ? 
            rightPos.transform.position : 
            leftPos.transform.position;
        
        if (Input.GetKeyDown("1") && points >= 1 )
        {
            points--;
            Instantiate(chickPrefab, pos, Quaternion.identity, null);
        }
        else if (Input.GetKeyDown("2") && points >= 3 )
        {
            points-=3;
            Instantiate(chickenPrefab, pos, Quaternion.identity, null);
        }
        else if (Input.GetKeyDown("3") && points >= 5 )
        {
            points-=5;
            Instantiate(chickHardPrefab, pos, Quaternion.identity, null);
        }

        enemyPointsNumber.text = points + "";
    }
}
