using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthBar : MonoBehaviour
{
    public Mortality mortality;

    public SpriteRenderer healthBar;

    public float initialScaleX;
    // Start is called before the first frame update
    void Start()
    {
        initialScaleX = healthBar.gameObject.transform.localScale.x;
    }

    // Update is called once per frame
    void Update()
    {
        
        float percentLeft = mortality.hp / mortality.maxHP;
        
        // set scale of filled red bar
        healthBar.gameObject.transform.localScale = new Vector3(
            initialScaleX*percentLeft,
            healthBar.gameObject.transform.localScale.y,
            healthBar.gameObject.transform.localScale.z);

        // reset position, so the left part of the filled bar is anchored to the left of the enire health bar
        float sizeX = healthBar.bounds.size.x;
        healthBar.gameObject.transform.localPosition = new Vector3(
            sizeX/2,
            healthBar.gameObject.transform.localPosition.y,
            healthBar.gameObject.transform.localPosition.z
        );
    }
}
