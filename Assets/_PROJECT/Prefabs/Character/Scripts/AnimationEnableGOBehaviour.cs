using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEnableGOBehaviour : StateMachineBehaviour
{
    public float normalizedTimeToEnableGO, normalizedTimeToDisableGO;
    
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    //override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (stateInfo.normalizedTime > normalizedTimeToEnableGO)
        { 
            GameObject goToEnable = animator.gameObject.transform.
                GetChild(animator.gameObject.transform.childCount - 1).gameObject;

            if (goToEnable.activeSelf == false)
            {
                goToEnable.SetActive(true);
            }
        }
        
        else if (stateInfo.normalizedTime > normalizedTimeToDisableGO)
        { 
            GameObject goToDisable = animator.gameObject.transform.
                GetChild(animator.gameObject.transform.childCount - 1).gameObject;

            if (goToDisable.activeSelf == true)
            {
                goToDisable.SetActive(false);
            }
        }
        
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        GameObject goToEnable = animator.gameObject.transform.
            GetChild(animator.gameObject.transform.childCount - 1).gameObject;

        goToEnable.SetActive(false);
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
