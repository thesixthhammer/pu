using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Chick : MonoBehaviour
{
    public AudioSource[] attackSfx;
    public Rigidbody2D body;
    [HideInInspector]
    public GameObject jumpTarget;
    public float jumpStrength;
    public float jumpIntervalMin, jumpIntervalMax;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(JumpCoroutine());
    }

    // Update is called once per frame
    IEnumerator JumpCoroutine()
    {
        while (true)
        {
            if (jumpTarget)
            {
                Vector2 jumpDirection = Vector2.zero;
                if (jumpTarget.transform.position.x < this.transform.position.x)
                {
                    // target is on the left
                    jumpDirection = new Vector2(-1, 1);
                }
                else
                {
                    // target is on the right
                    jumpDirection = new Vector2(1, 1);
                }

                int index = Random.Range(0, attackSfx.Length);
                attackSfx[index].pitch = Random.Range(0.8f, 1.2f);
                attackSfx[index].Play();
                body.AddForce(jumpDirection * jumpStrength, ForceMode2D.Impulse);
            }

            yield return new WaitForSeconds(Random.Range(jumpIntervalMin, jumpIntervalMax));
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "player")
        {
            jumpTarget = other.gameObject;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "player")
        {
            jumpTarget = null;
        }
    }
}
