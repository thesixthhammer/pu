using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAi : MonoBehaviour
{
    public float movementSpeed;
    public float closestDistanceToPlayer = 2;
    public Animator enemyAnimator;
    public SpriteRenderer spriteRenderer;
    public Rigidbody2D rigidbody2D;
    public Attack attack;
    private GameObject player;
    private void FixedUpdate()
    {
        if (player != null)
        {
            float distanceToPlayer = Mathf.Abs(player.transform.position.x - this.transform.position.x);
            if (distanceToPlayer < closestDistanceToPlayer)
            {
                if (player.transform.position.x > this.transform.position.x)
                {
                    attack.ShootRight();
                }
                else
                {
                    attack.ShootLeft();
                }
                // chicken should stop 
                enemyAnimator.SetBool("isMoving",false);
            }
            else
            {
                // chicken should move towards player
                if (player.transform.position.x > this.transform.position.x)
                {
                    rigidbody2D.AddForce(new Vector2(1, 0) * movementSpeed);
                    spriteRenderer.flipX = false;
                    // move right
                }
                else
                {
                    rigidbody2D.AddForce(new Vector2(-1, 0) * movementSpeed);
                    spriteRenderer.flipX = true;
                    // move left
                }

                enemyAnimator.SetBool("isMoving", true);
            }
        }
        else
        {
            enemyAnimator.SetBool("isMoving",false);
            // chicken should chill in one place
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "player")
        {
            player = other.gameObject;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "player")
        {
            player = null;
        }
    }
}
